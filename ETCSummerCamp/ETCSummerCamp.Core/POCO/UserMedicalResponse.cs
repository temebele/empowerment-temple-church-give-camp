//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ETCSummerCamp.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserMedicalResponse
    {
        public UserMedicalResponse()
        {
           
        }
    
        public int UserMedicalResponseId { get; set; }
        public Nullable<bool> UserResponse { get; set; }
    
        public Questionaire Question { get; set; }
      
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
