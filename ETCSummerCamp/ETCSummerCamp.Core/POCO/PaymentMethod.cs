//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ETCSummerCamp.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentMethod
    {
        public PaymentMethod()
        {
            this.UserPayments = new HashSet<UserPayment>();
        }
    
        public int PaymentMethodId { get; set; }
        public string PaymentMethodName { get; set; }
    
        public virtual ICollection<UserPayment> UserPayments { get; set; }
    }
}
