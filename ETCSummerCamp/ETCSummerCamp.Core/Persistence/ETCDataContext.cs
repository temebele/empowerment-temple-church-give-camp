﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ETCDataContext.cs" company="">
//   
// </copyright>
// <summary>
//   The etc data context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ETCSummerCamp.Core.Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Linq;

    using Dapper;

    using ETCSummerCamp.Core.Entities;

    using Sac.Core.DomainServices.Implements;

    /// <summary>
    ///     The etc data context.
    /// </summary>
    public class ETCDataContext
    {
        #region Fields

        /// <summary>
        ///     The connectionstring.
        /// </summary>
        private readonly string connectionstring;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ETCDataContext" /> class.
        /// </summary>
        public ETCDataContext()
        {
            this.connectionstring = ConfigurationManager.ConnectionStrings["ETCDataContext"].ConnectionString;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add update user.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        public void AddUpdateUser(User user)
        {
            using (var sqlConnection = new SqlConnection(this.connectionstring))
            {
                sqlConnection.Open();

                AddUpdateBasicUserData(user, sqlConnection);

                AddUpdateUserDetail(user, sqlConnection);

                // Add Contacts
                AddUpdateContacts(user, sqlConnection);

                AddUpdateMedicalResponse(user, sqlConnection);

                AddUpdatePayments(user, sqlConnection);

                sqlConnection.Close();
            }
        }

        /// <summary>
        ///     The get contact types.
        /// </summary>
        /// <returns>
        ///     The <see cref="List" />.
        /// </returns>
        public List<ContactType> GetContactTypes()
        {
            List<ContactType> ContactTypeList;
            using (var sqlConnection = new SqlConnection(this.connectionstring))
            {
                sqlConnection.Open();
                ContactTypeList =
                    sqlConnection.Query<ContactType>(@"SELECT ContactTypeId,ContactTypeName FROM ContactTypes").ToList();
            }

            return ContactTypeList;
        }

        /// <summary>
        ///     The get payment methods.
        /// </summary>
        /// <returns>
        ///     The <see cref="List" />.
        /// </returns>
        public List<PaymentMethod> GetPaymentMethods()
        {
            List<PaymentMethod> PaymentMethodList;
            using (var sqlConnection = new SqlConnection(this.connectionstring))
            {
                sqlConnection.Open();
                PaymentMethodList =
                    sqlConnection.Query<PaymentMethod>(@"SELECT PaymentMethodId,PaymentMethodName FROM PaymentMethods")
                        .ToList();
            }

            return PaymentMethodList;
        }

        /// <summary>
        ///     The get questionaires.
        /// </summary>
        /// <returns>
        ///     The <see cref="List" />.
        /// </returns>
        public List<Questionaire> GetQuestionaires()
        {
            List<Questionaire> QuesionaireList;
            using (var sqlConnection = new SqlConnection(this.connectionstring))
            {
                sqlConnection.Open();
                QuesionaireList =
                    sqlConnection.Query<Questionaire>(
                        @"SELECT QuestionaireId,QuestionaireDescripton FROM Questionaires").ToList();
            }

            return QuesionaireList;
        }

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        public User GetUser(int userId)
        {
            User user = null;

            using (var sqlConnection = new SqlConnection(this.connectionstring))
            {
                sqlConnection.Open();

                user =
                    sqlConnection.Query<User>(@"SELECT * FROM Users WHERE UserId = @userId", new { UserId = userId })
                        .FirstOrDefault();
                if (user != null)
                {
                    user.UserDetail =
                        sqlConnection.Query<UserDetail>(
                            @"SELECT * FROM UserDetails WHERE UserId = @userId", 
                            new { UserId = userId }).FirstOrDefault();
                    user.UserPayments =
                        sqlConnection.Query<UserPayment>(
                            @"SELECT * FROM UserPayments WHERE UserId = @userId", 
                            new { UserId = userId }).ToList();

                    // user. = sqlConnection.Query<UserResponseDescription>(@"SELECT * FROM UserMedicalResponses WHERE UserId = @userId", new { UserId = userId }).FirstOrDefault();
                    user.Contacts =
                        sqlConnection.Query<Contact>(
                            @"SELECT * FROM Contacts WHERE UserId = @userId", 
                            new { UserId = userId }).ToList();
                }

                sqlConnection.Close();

                return user;
            }

            return user;
        }

        /// <summary>
        ///     The get users.
        /// </summary>
        /// <returns>
        ///     The <see cref="List" />.
        /// </returns>
        public List<User> GetUsers()
        {
            var userList = new List<User>();
            using (var sqlConnection = new SqlConnection(this.connectionstring))
            {
                sqlConnection.Open();

                IEnumerable<User> products =
                    sqlConnection.Query<User, UserDetail, User>(
                        @"select a.*,s.*,s.Email,s.BirthDate,s.StreetAddress,s.City,s.State,s.Zip,
s.Phone1,s.Phone2,s.Age,s.Gender,s.Grade,s.TShirtSize,s.Ethnicity,s.Parent
from Users a inner join UserDetails s
on a.UserId = s.UserId", 
                        (a, s) =>
                        {
                            a.UserDetail = s;
                            return a;
                        }, 
                        splitOn: "UserId").AsQueryable().ToList();

                // (a, s) =>
                // {
                // a.UserDetail = s;
                // return a;
                // }); // use splitOn, if the id field is not Id or ID
                sqlConnection.Close();

                return products.Any() ? products.ToList() : userList;
            }

            return userList;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The add update basic user data.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="sqlConnection">
        /// The sql connection.
        /// </param>
        private static void AddUpdateBasicUserData(User user, SqlConnection sqlConnection)
        {
            if (user.UserId == 0)
            {
                user.SaltKey = Cryptographer.CreateSalt();
                user.Password = Cryptographer.GetPasswordHash(user.Password, user.SaltKey);
                user.CreatedDate = DateTime.UtcNow.ToShortDateString();
                user.ModifiedDate = DateTime.UtcNow.ToShortDateString();

                int id = sqlConnection.Query<int>(@"
                            INSERT Users(UserName, Password, SaltKey, CreatedDate,ModifiedDate)
                            VALUES (@UserName, @Password,@SaltKey,@CreatedDate,@ModifiedDate)
                            SELECT cast(scope_identity() as int)", user).First();

                // User Details
                user.UserId = id;
                user.UserDetail.UserId = id;
            }
            else
            {
                sqlConnection.Execute(@"
                            UPDATE Users Password = @Password, SaltKey = @SaltKey, CreatedDate = @CreatedDate, ModifiedDate = @ModifiedDate)
                            ", user);
                
            }
        }

        /// <summary>
        /// The add update contacts.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="sqlConnection">
        /// The sql connection.
        /// </param>
        private static void AddUpdateContacts(User user, SqlConnection sqlConnection)
        {
            if (user.Contacts.Any())
            {
                foreach (Contact c in user.Contacts)
                {
                    if (c.User.UserId == 0)
                    {
                        c.UserId = user.UserId;
                    }
                    
                    if (c.ContactId == 0)
                    {
                        sqlConnection.Execute(@"
                            INSERT Contacts(UserId, ContactTypeId, FirstName, MiddleName, LastName, Phone, Relationship, SpecialInstruction, Doctor, InsuranceCarrier, PolicyNumber)
                            values (@UserId,@ContactTypeId, @FirstName,@MiddleName, @LastName,@Phone,@Relationship,@SpecialInstruction,@Doctor,@InsuranceCarrier,@PolicyNumber)                         
                        ", new
                        {
                            UserId = c.User.UserId,
                            ContactTypeId = c.ContactTypeId,
                            FirstName = c.FirstName,
                            MiddleName = c.LastName,
                            Phone = c.Phone,
                            Relationship = c.Relationship,
                            SpecialInstructions = c.SpecialInstruction,
                            Doctor = c.Doctor,
                            InsuranceCarrier = c.InsuranceCarrier,
                            PolicyNumber = c.PolicyNumber
                        });
                    }
                    else
                    {
                        sqlConnection.Execute(@"
                            UPDATE Contacts UserId = @UserId, ContactTypeId = @ContactTypeId, FirstName = @FirstName, MiddleName = @MiddleName, LastName = @LastName, Phone = @Phone, Relationship = @Relationship, SpecialInstruction = @SpecialInstruction, Doctor = @Doctor, InsuranceCarrier = @InsuranceCarrier, PolicyNumber = @PolicyNumber,
                            State=@State, Zip=@Zip,Phone1=@Phone1, Phone2=@Phone2,Age=@Age,Gender=@Gender,Grade=@Grade,TShirtSize=@TShirtSize,Ethnicity=@Ethnicity,Parent=@Parent)
                            ",    new {
                            UserId = c.User.UserId,
                            ContactTypeId = c.ContactTypeId,
                            FirstName = c.FirstName,
                            MiddleName = c.MiddleName,
                            Phone = c.Phone,
                            Relationship = c.Relationship,
                            SpecialInstruction = c.SpecialInstruction,
                            Doctor = c.Doctor,
                            InsuranceCarrier = c.InsuranceCarrier,
                            PolicyNumber = c.PolicyNumber
                        });
                    }
                }
            }
        }

        /// <summary>
        /// The add update medical response.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="sqlConnection">
        /// The sql connection.
        /// </param>
        private static void AddUpdateMedicalResponse(User user, SqlConnection sqlConnection)
        {
            // Medical Responses
            if (user.UserMedicalResponses.Any())
            {

                foreach (UserMedicalResponse m in user.UserMedicalResponses)
                {
                    if (m.User.UserId == 0)
                    {
                        m.UserId = user.UserId;
                    }
                    if (m.UserMedicalResponseId == 0)
                    {
                        sqlConnection.Execute(@"
                            INSERT [UserMedicalResponses]([UserResponse], [UserId], [QuestionaireId])
                            values (@UserResponse,@UserId,  @QuestionaireId)                         
                        ", new { m.UserResponse, m.UserId, m.Question.QuestionaireId });
                    }
                    else
                    {
                        sqlConnection.Execute(@"
                            UPDATE UserMedicalResponses UserResponse = @UserResponse , QuestionaireId = @QuestionaireId)
                            ", new { m.UserResponse, m.Question.QuestionaireId });
                    }
                }
            }
        }

        /// <summary>
        /// The add update payments.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="sqlConnection">
        /// The sql connection.
        /// </param>
        private static void AddUpdatePayments(User user, SqlConnection sqlConnection)
        {
            // Payments
            if (user.UserPayments.Any())
            {
                foreach (UserPayment p in user.UserPayments)
                {
                    if (p.PaymentMethodId == 0)
                    {
                        sqlConnection.Execute(@"
                            INSERT UserPayments([UserId], [CamperFirstName], [CamperMiddleName], [CamperLastName], [ParentFirstName], [ParentLastName], [ParentMiddleName])
                            values (@UserId,@CamperFirstName, @CamperMiddleName, @CamperLastName, @ParentFirstName, @ParentLastName,@ParentMiddleName)                         
                        ", p);
                    }
                    else
                    {
                        sqlConnection.Execute(@"
                            UPDATE UserPayments UserId = @UserId , CamperFirstName = @CamperFirstName, CamperMiddleName=@CamperMiddleName, CamperLastName=@CamperLastName, 
                            ParentFirstName=@ParentFirstName, ParentLastName=@ParentLastName, ParentMiddleName=@ParentMiddleName)
                            ", p);
                    }
                }
            }
        }

        /// <summary>
        /// The add update user detail.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="sqlConnection">
        /// The sql connection.
        /// </param>
        private static void AddUpdateUserDetail(User user, SqlConnection sqlConnection)
        {
            if (user.UserDetail.UserDetailId == 0)
            {
                user.UserDetail.UserId = user.UserId;
                sqlConnection.Execute(@"
                            INSERT USERDETAILS(UserId, Name, Email, BirthDate, StreetAddress, City,State, Zip,Phone1, Phone2,Age,Gender,Grade,TShirtSize,Ethnicity,Parent)
                            values (@UserId,@Name, @Email, @BirthDate, @StreetAddress, @City,@State, @Zip,@Phone1, @Phone2,@Age,@Gender,@Grade,@TShirtSize,@Ethnicity,@Parent)                         
                        ", user.UserDetail);
            }
            else
            {
                sqlConnection.Execute(@"
                            UPDATE USERDETAILS UserId = @UserId , Name, Email=@Email, BirthDate=@BirthDate, StreetAddress=@StreetAddress, City=@City,
                            State=@State, Zip=@Zip,Phone1=@Phone1, Phone2=@Phone2,Age=@Age,Gender=@Gender,Grade=@Grade,TShirtSize=@TShirtSize,Ethnicity=@Ethnicity,Parent=@Parent)
                            ", user.UserDetail);
            }
        }

        #endregion
    }
}