﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthenticationService.cs" company="">
//   
// </copyright>
// <summary>
//   The authentication service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ETCSummerCamp.Core.Services.Implementation
{
    using System;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    using ETCSummerCamp.Core.Entities;
    using ETCSummerCamp.Core.Persistence;

    using Sac.Core.DomainServices.Implements;
    using System.Web;

    /// <summary>
    ///     The authentication service.
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        #region Fields

        /// <summary>
        ///     The context.
        /// </summary>
        private readonly ETCDataContext context;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public AuthenticationService(ETCDataContext context)
        {
            this.context = context;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The authenticate.
        /// </summary>
        /// <param name="userName">
        /// The user Name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="rememberMe">
        /// The remember Me.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Authenticate(string userName, string password, bool rememberMe)
        {
            User user = null;

            try
            {
                user=this.context.GetUsers().Find(u => u.UserName == userName);
                //user = this.context.Users.First(u => u.UserName == userName);
            }
            catch (Exception ex)
            {
                throw new Exception("The requested user could not be found.", ex);
            }

            // Fianlly check if the passwords match
            if (null != user)
            {
                if (user.Password == Cryptographer.ComputeHash(password + user.SaltKey))
                {
                    // HttpContext.Current.User.Identity.Name = userName;
                    // var identity = UserIdentity(userName);
                    // var principal = newUserPrincipal(identity);
                    // Thread.CurrentPrincipal = principal;
                    // GenericPrincipal MyPrincipal = new GenericPrincipal();
                    // IPrincipal Identity = (IPrincipal)MyPrincipal;
                    // HttpContext.User = Identity;
                    HttpContext.Current.Session["LoggedInUserID"] = user.UserId;
                    HttpContext.Current.Session["LoggedInUserName"] = user.UserName;
                    return true;
                }

                throw new Exception("The supplied user name and password do not match.");
            }

            return false;
        }

        /// <summary>
        /// The compute hash.
        /// </summary>
        /// <param name="valueToHash">
        /// The value to hash.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ComputeHash(string valueToHash)
        {
            HashAlgorithm algorithm = SHA512.Create();
            byte[] hash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(valueToHash));

            return Convert.ToBase64String(hash);
        }

        /// <summary>
        /// The get password hash.
        /// </summary>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="salt">
        /// The salt.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetPasswordHash(string password, string salt)
        {
            return this.ComputeHash(password + salt);
        }

        

        #endregion
    }
}