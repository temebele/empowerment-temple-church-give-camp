﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegistrationService.cs" company="">
//   
// </copyright>
// <summary>
//   The registration service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ETCSummerCamp.Core.Services.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Net.Mail;
    using System.Text;

    using ETCSummerCamp.Core.Entities;
    using ETCSummerCamp.Core.Persistence;

    using Sac.Core.DomainServices.Implements;

    /// <summary>
    ///     The registration service.
    /// </summary>
    public class RegistrationService : IRegistrationService
    {
        #region Fields

        /// <summary>
        ///     The context.
        /// </summary>
        private readonly ETCDataContext context;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistrationService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public RegistrationService(ETCDataContext context)
        {
            this.context = context;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add contact.
        /// </summary>
        /// <param name="contacts">
        /// The contacts.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void AddContact(IList<Contact> contacts)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The create user.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public User AddUpdateUser(User user)
        {
            try
            {
                    
                    context.AddUpdateUser(user);
          

//                string emailAddress = user.UserDetail.Email;
//                var sb = new StringBuilder();
//                sb.Append("Thanks for registering for Summer Camp!");
//                this.SendEmail(emailAddress, sb.ToString(), "Registration Confirmation");
            }
            catch (Exception ex)
            {
                throw new Exception("User Creation Failed");
            }

            return null;
        }

        /// <summary>
        /// The save payment method.
        /// </summary>
        /// <param name="paymentMethods">
        /// The payment methods.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void SavePaymentMethod(IList<PaymentMethod> paymentMethods)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The save questionnaire.
        /// </summary>
        /// <param name="questionaires">
        /// The questionaires.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void SaveQuestionnaire(IList<Questionaire> questionaires)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The send email.
        /// </summary>
        /// <param name="emailAddress">
        /// The email address.
        /// </param>
        /// <param name="content">
        /// The content.
        /// </param>
        /// <param name="subject">
        /// The subject.
        /// </param>
        public void SendEmail(string emailAddress, string content, string subject)
        {
            string fromAddressReset = ConfigurationManager.AppSettings["FromAddress"];
            var mail = new MailMessage(fromAddressReset, emailAddress) { Subject = subject, Body = content };
            mail.IsBodyHtml = true;
            mail.BodyEncoding = Encoding.ASCII;
            string smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
            var smtpClient = new SmtpClient(smtpServer);
            smtpClient.Send(mail);
        }

        public IList<Questionaire> GetQuestionaires()
        {
            return context.GetQuestionaires();
        }

        public IList<PaymentMethod> GetPaymentMethods()
        {
            return context.GetPaymentMethods();
        }

        public IList<ContactType> GetContactTypes()
        {
            return context.GetContactTypes();
        }

        #endregion



    }
}