﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserService.cs" company="">
//   
// </copyright>
// <summary>
//   The user service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using ETCSummerCamp.Core.Services.Contract;

namespace ETCSummerCamp.Core.Services.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ETCSummerCamp.Core.Entities;
    using ETCSummerCamp.Core.Persistence;

    /// <summary>
    ///     The user service.
    /// </summary>
    public class UserService : IUserService
    {
        #region Fields

        /// <summary>
        ///     The context.
        /// </summary>
        private readonly ETCDataContext context;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="dataContext">
        /// The data context.
        /// </param>
        public UserService(ETCDataContext dataContext)
        {
            this.context = dataContext;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The update user.
        /// </summary>
        /// <param name="newUser">
        /// The new user.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void AddUser(User newUser)
        {
            var user = new User();
           
        }

        /// <summary>
        /// Get User
        /// </summary>
        /// <param name="userId">
        /// </param>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        public User GetUser(int userId)
        {
            return this.context.GetUser(userId);
        }

        /// <summary>
        ///     The get users.
        /// </summary>
        /// <returns>
        ///     The <see cref="IList" />.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IList<User> GetUsers()
        {
            return this.context.GetUsers();
            //return this.context.Users.ToList();
        }

        public void UpdateUser(User newUser)
        {
            
           // var user = this.context.Users.Find(newUser.UserId);

        }

        #endregion
    }
}