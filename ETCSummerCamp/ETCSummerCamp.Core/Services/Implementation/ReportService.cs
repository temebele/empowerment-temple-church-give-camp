﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using ETCSummerCamp.Core.Entities;
using ETCSummerCamp.Core.Services.Contract;
using ETCSummerCamp.Core.ViewModels;

namespace ETCSummerCamp.Core.Services.Implementation
{
    public class ReportService : IReportService
    {
        private readonly IUserService _userService;

        public ReportService(IUserService userService)
        {
            _userService = userService;
        }

        public IEnumerable<UserReportItem> GetUserList()
        {
            var userList= _userService.GetUsers();
            if (userList.Any())
            {
                return userList.Select(k => new UserReportItem
                {
                    User = k,
                    Contact = k.Contacts.FirstOrDefault(),
                    UserDetail = k.UserDetail
                });
            }

            return new List<UserReportItem>();
        }
    }
}
