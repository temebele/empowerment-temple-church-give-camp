﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETCSummerCamp.Core.Services
{
    using System.Net.Security;

    public interface IAuthenticationService
    {

        bool Authenticate(string userName, string password, bool persistCookie);
    }
}
