﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserService.cs" company="">
//   
// </copyright>
// <summary>
//   The UserService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ETCSummerCamp.Core.Services.Contract
{
    using System.Collections.Generic;

    using ETCSummerCamp.Core.Entities;

    /// <summary>
    /// The UserService interface.
    /// </summary>
    public interface IUserService
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        User GetUser(int userId);

        /// <summary>
        /// The get users.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        IList<User> GetUsers();

        /// <summary>
        /// The update user.
        /// </summary>
        /// <param name="newUser">
        /// The new user.
        /// </param>
        void AddUser(User newUser);

        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="newUser">
        /// The new user.
        /// </param>
        void UpdateUser(User updatedUser);

        #endregion
    }
}