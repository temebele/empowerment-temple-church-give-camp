﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ETCSummerCamp.Core.ViewModels;

namespace ETCSummerCamp.Core.Services.Contract
{
    public interface IReportService
    {
        IEnumerable<UserReportItem> GetUserList();
    }
}
