﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETCSummerCamp.Core.Services
{
    using ETCSummerCamp.Core.Entities;

    public interface IRegistrationService
    {
        User AddUpdateUser(User user);

        void SavePaymentMethod(IList<PaymentMethod> paymentMethods);

        void SaveQuestionnaire(IList<Questionaire> questionaires);

        void AddContact(IList<Contact> contacts);

        IList<Questionaire> GetQuestionaires();

        IList<PaymentMethod> GetPaymentMethods();

        IList<ContactType> GetContactTypes();
    }
}
