﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ETCSummerCamp.Core.Entities;

namespace ETCSummerCamp.Core.ViewModels
{
    public class UserReportItem
    {
        public User User { get; set; }
        public Contact Contact { get; set; }
        public UserDetail UserDetail { get; set; }

        public string CompleteAddress
        {
            get
            {
                    return UserDetail!=null ? String.Format("{0} {1} {2} {3}", UserDetail.StreetAddress, UserDetail.State, UserDetail.City,
                        UserDetail.Zip):String.Empty;
            }
        }

        public string ContactName
        {
            get
            {
                if (Contact != null)
                {
                    return (Contact.LastName ?? "") + "," + (Contact.FirstName ?? "");
                }
                return "";
            }
        }
    }
}
