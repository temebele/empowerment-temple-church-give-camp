﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using ETCSummerCamp.Core.Services.Contract;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using ETCSummerCamp.Filters;
using ETCSummerCamp.Models;
using ETCSummerCamp.Core.Services;
using CoreUser = ETCSummerCamp.Core.Entities.User;
using ETCSummerCamp.Core.Entities;

namespace ETCSummerCamp.Controllers
{
    using ETCSummerCamp.Core.Services;

    using WebAndAPILayer.Filters;

    [Authorize]
    public class AccountController : Controller
    {
        public IAuthenticationService authenticationService;
        public IRegistrationService registrationService;
        private readonly IUserService _userService;
        public AccountController(IAuthenticationService authenticationService, IRegistrationService registrationService, IUserService userService)
        {
            this.authenticationService = authenticationService;
            this.registrationService = registrationService;
            this._userService = userService;
        }

        private RegistrationModel GetTestData()
        {
            StepOneModel sModel = new StepOneModel
                {
                    Address = "1234 Snowden River Park",
                    Age = 20,
                    AlternativePhone = "4107105123",
                    BirthDate = DateTime.Now,
                    City = "Baltimore",
                    Email = "registration@test.com",
                    Ethnicity = "Blue",
                    Gender="Unknown",
                    Grade = "K",
                    HomePhone = "5415163135",
                    Parent = "Joe",
                    ShirtSize = "Small",
                    UserName = "TJoe",
                    State = "NY",
                    StudentName = "Blue Jay",
                    ZipCode = "21051"
                };

            MedicalInfoModel mModel = new MedicalInfoModel
                {
                    Doctor = "Doctor Doctor",
                    Insurance = "State Farm",
                    MedicalReason = "Sick",
                    Phone = "53633535",
                    PolicyNumber = "53535353",
                    Questionaires = new Collection<QuestionaireModel>
                        {
                            new QuestionaireModel
                                {
                                    QuestionDescription = "Question 1",
                                    QuestionResponse = true
                                },
                            new QuestionaireModel
                                {
                                    QuestionDescription = "Question 2",
                                    QuestionResponse = false
                                }
                        }
                };

            ContactsModel cModel = new ContactsModel
                {
                    EmergencyContact = new ContactModel
                        {
                            SpecialInstructions = "Don't touch",
                            Name = "Bob",
                            Phone = "353525252",
                            Relationship = "Sister"
                        },
                };

            cModel.Contacts[0] = new ContactModel
                {
                    SpecialInstructions = "Do touch",
                    Name = "Bob3",
                    Phone = "353525252",
                    Relationship = "Si1ster"
                };

            cModel.Contacts[1] = new ContactModel
                {
                    SpecialInstructions = "Don't touch",
                    Name = "Bob2",
                    Phone = "3535a25252",
                    Relationship = "Sis4ter"
                };

            PaymentModel pModel = new PaymentModel
                {
                    Camper = new SignatureModel
                        {
                            FirstName = "Joe",
                            LastName = "Bill",
                            MiddleName = "A"
                        },
                    Parent = new SignatureModel
                        {
                            FirstName = "J4e",
                            LastName = "B1l",
                            MiddleName = "Ab"
                        },
                    PaymentPlan = 3
                };

            RegistrationModel rModel = new RegistrationModel();
            rModel.Payment = pModel;
            rModel.StepOne = sModel;
            rModel.ContactInfo = cModel;
            rModel.MedicalInfo = mModel;

            return rModel;
        }

        private RegistrationModel GetUserRegistrationModel(int userId)
        {
            var user = _userService.GetUser(userId);
            RegistrationModel rModel = new RegistrationModel();

            rModel.StepOne = new StepOneModel
                {
                    Address = user.UserDetail.StreetAddress,
                    Age = user.UserDetail.Age.HasValue ? user.UserDetail.Age.Value : 0,
                    AlternativePhone = user.UserDetail.Phone2,
                    BirthDate = user.UserDetail.BirthDate,
                    City = user.UserDetail.City,
                    Email = user.UserDetail.Email,
                    Ethnicity = user.UserDetail.Ethnicity,
                    Gender = user.UserDetail.Gender,
                    Grade = user.UserDetail.Grade,
                    HomePhone = user.UserDetail.Phone1,
                    Parent = user.UserDetail.Parent,
                    ShirtSize = user.UserDetail.TShirtSize,
                    UserName = user.UserName,
                    State = user.UserDetail.State,
                    StudentName = user.UserDetail.Name,
                    ZipCode = user.UserDetail.Zip
                };

            if (user.Contacts.Any())
            {
                foreach (var contact in user.Contacts)
                {
                    if (contact.ContactTypeId == 2)
                    {
                        rModel.MedicalInfo = new MedicalInfoModel
                        {
                            Doctor = contact.Doctor,
                            Insurance = contact.InsuranceCarrier,
                            MedicalReason = contact.SpecialInstruction,
                            Phone = contact.Phone,
                            PolicyNumber = contact.PolicyNumber
                        };
                    }

                    if (contact.ContactTypeId == 1)
                    {
                        rModel.ContactInfo = new ContactsModel
                        {
                            EmergencyContact = new ContactModel
                            {
                                SpecialInstructions = contact.SpecialInstruction,
                                Name = contact.LastName+"," + contact.FirstName,
                                Phone = contact.Phone,
                                Relationship = contact.Relationship   
                            }
                        };
                    }

                    if (contact.ContactTypeId != 1 && contact.ContactTypeId!=2)
                    {
                        rModel.ContactInfo.Contacts.Add(new ContactModel
                        {
                            SpecialInstructions = contact.SpecialInstruction,
                            Name = contact.LastName + "," + contact.FirstName,
                            Phone = contact.Phone,
                            Relationship = contact.Relationship
                        });
                    }
                }
            }

            if (rModel.MedicalInfo != null && user.UserMedicalResponses.Any())
            {
                rModel.MedicalInfo.Questionaires = new Collection<QuestionaireModel>();
                var medicalResponse = user.UserMedicalResponses.FirstOrDefault();

                rModel.MedicalInfo.Questionaires.Add(
                    new QuestionaireModel
                    {
                        QuestionDescription = medicalResponse.Question.QuestionaireDescripton,
                        QuestionResponse = medicalResponse.UserResponse.HasValue ? medicalResponse.UserResponse.Value : false
                    }
                    );


            }

            if (user.UserPayments.Any())
            {
                var payementObject = user.UserPayments.FirstOrDefault();
                rModel.Payment = new PaymentModel
                {
                    Camper = new SignatureModel
                    {
                        FirstName = payementObject.CamperFirstName,
                        LastName = payementObject.CamperLastName,
                        MiddleName = payementObject.CamperMiddleName
                    },
                    Parent = new SignatureModel
                    {
                        FirstName = payementObject.ParentFirstName,
                        LastName = payementObject.ParentLastName,
                        MiddleName = payementObject.ParentMiddleName
                    },
                    PaymentPlan = payementObject.PaymentMethodId
                };
            }
            

            return rModel;
        }

        private Collection<QuestionaireModel> RetrieveQuestionaires() {          

            Collection<QuestionaireModel> result = new Collection<QuestionaireModel>();
            foreach (Questionaire quest in this.registrationService.GetQuestionaires())
            {
                result.Add(new QuestionaireModel
                {
                    QuestionDescription = quest.QuestionaireDescripton,
                    QuestionId = quest.QuestionaireId
                });
            }

            return result;
        }

        private Collection<PaymentMethodModel> RetrievePaymentMethods()
        {
            Collection<PaymentMethodModel> result = new Collection<PaymentMethodModel>();
            foreach (PaymentMethod quest in this.registrationService.GetPaymentMethods())
            {
                result.Add(new PaymentMethodModel
                {
                    Id = quest.PaymentMethodId,
                    Description = quest.PaymentMethodName
                });
            }

            return result;
        }

        private RegistrationModel GetDefaultData()
        {
            StepOneModel sModel = new StepOneModel();
            MedicalInfoModel mModel = new MedicalInfoModel();
            ContactsModel cModel = new ContactsModel();
            PaymentModel pModel = new PaymentModel();
            mModel.Questionaires = RetrieveQuestionaires();

            RegistrationModel rModel = new RegistrationModel();
            rModel.Payment = pModel;
            rModel.StepOne = sModel;
            rModel.ContactInfo = cModel;
            rModel.MedicalInfo = mModel;

            return rModel;
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            if (Session["LoggedInUserId"] != null)
            {
                var userRegistrationObject = GetUserRegistrationModel((int)Session["LoggedInUserId"]);
                return View(userRegistrationObject);
            }
   
            return View(GetDefaultData());
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult JsonLogin(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (authenticationService.Authenticate(model.UserName, model.Password, persistCookie: model.RememberMe))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    return RedirectToAction("", "WelcomePackage");
                    //return Json(new { success = true, redirect = returnUrl });
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }
            return RedirectToAction("Index", "Home");
            // return Json(new { errors = GetErrorsFromModelState() });
        }

        //
        // POST: /Account/LogOff



        [HttpPost]
        [AllowAnonymous]
        public JsonResult SaveStepOne(RegistrationModel rModel, string returnUrl)
        {
            
            if (ModelState.IsValid)
            {
                CoreUser cUser = rModel.GetPocoObject();                      
                this.registrationService.AddUpdateUser(cUser);
                return Json(new { success = true, redirect = returnUrl }); ;
            }

            // If we got this far, something failed
            var errors = GetErrorsFromModelState();
            return Json(new { errors });
        }

        //
        // POST: /Account/LogOff

        [HttpGet]
        public ActionResult LogOff()
        {
            Session.Clear();

            
            return RedirectToAction("Index", "Home");
        }

        //
        // POST: /Account/JsonRegister
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult JsonRegister(RegisterModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    WebSecurity.Login(model.UserName, model.Password);

                    // InitiateDatabaseForNewUser(model.UserName);

                    FormsAuthentication.SetAuthCookie(model.UserName, createPersistentCookie: false);
                    return Json(new { success = true, redirect = returnUrl });
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed
            return Json(new { errors = GetErrorsFromModelState() });
        }

        //        /// <summary>
        //        /// Initiate a new todo list for new user
        //        /// </summary>
        //        /// <param name="userName"></param>
        //        private static void InitiateDatabaseForNewUser(string userName)
        //        {
        //            TodoItemContext db = new TodoItemContext();
        //            TodoList todoList = new TodoList();
        //            todoList.UserId = userName;
        //            todoList.Title = "My Todo List #1";
        //            todoList.Todos = new List<TodoItem>();
        //            db.TodoLists.Add(todoList);
        //            db.SaveChanges();
        //
        //            todoList.Todos.Add(new TodoItem() { Title = "Todo item #1", TodoListId = todoList.TodoListId, IsDone = false });
        //            todoList.Todos.Add(new TodoItem() { Title = "Todo item #2", TodoListId = todoList.TodoListId, IsDone = false });
        //            db.SaveChanges();
        //        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private IEnumerable<string> GetErrorsFromModelState()
        {
            return ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage));
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}