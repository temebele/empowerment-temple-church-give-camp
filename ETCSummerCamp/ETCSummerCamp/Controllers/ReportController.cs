﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ETCSummerCamp.Core.Entities;
using ETCSummerCamp.Core.Services.Contract;
using ETCSummerCamp.Core.ViewModels;

namespace ETCSummerCamp.Controllers
{
    public class ReportController : Controller
    {
        private IReportService _reportService;
        private IUserService _userService;

        public ReportController(IReportService reportService, IUserService userService)
        {
            _reportService = reportService;
            _userService = userService;
        }

        public ActionResult Index(int? rowCount)
        {
            var results = _reportService.GetUserList();
            if (rowCount.HasValue)
                ViewData["rowCount"] = rowCount;
            else
            {
                ViewData["rowCount"] = 20;
            }
            return View("Report", results);
        }

        [HttpGet]
        public ActionResult MoreUserDetail(int id)
        {
            var userObject  = _userService.GetUser(id);
            return PartialView("MoreUserDetail", userObject);
        }
    }
}
