﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StructureMapDependencyResolver.cs" company="">
//   
// </copyright>
// <summary>
//   The structure map dependency resolver.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using StructureMap;

/// <summary>
/// The structure map dependency resolver.
/// </summary>
public class StructureMapDependencyResolver : IDependencyResolver
{
    #region Fields

    /// <summary>
    /// The _container.
    /// </summary>
    private readonly IContainer _container;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="StructureMapDependencyResolver"/> class.
    /// </summary>
    /// <param name="container">
    /// The container.
    /// </param>
    public StructureMapDependencyResolver(IContainer container)
    {
        this._container = container;
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// The get service.
    /// </summary>
    /// <param name="serviceType">
    /// The service type.
    /// </param>
    /// <returns>
    /// The <see cref="object"/>.
    /// </returns>
    public object GetService(Type serviceType)
    {
        if (serviceType.IsAbstract || serviceType.IsInterface)
        {
            return this._container.TryGetInstance(serviceType);
        }

        return this._container.GetInstance(serviceType);
    }

    /// <summary>
    /// The get services.
    /// </summary>
    /// <param name="serviceType">
    /// The service type.
    /// </param>
    /// <returns>
    /// The <see cref="IEnumerable"/>.
    /// </returns>
    public IEnumerable<object> GetServices(Type serviceType)
    {
        return this._container.GetAllInstances<object>().Where(s => s.GetType() == serviceType);
    }

    #endregion
}