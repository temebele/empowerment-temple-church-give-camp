﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="">
//   
// </copyright>
// <summary>
//   The mvc application.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using ETCSummerCamp.Core.Services.Contract;

namespace ETCSummerCamp
{
    using System.Configuration;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    using Autofac;
    using Autofac.Integration.Mvc;

    using ETCSummerCamp.Controllers;
    using ETCSummerCamp.Core.Persistence;
    using ETCSummerCamp.Core.Services;
    using ETCSummerCamp.Core.Services.Implementation;

    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    /// <summary>
    /// The mvc application.
    /// </summary>
    public class MvcApplication : HttpApplication
    {
        #region Methods

        /// <summary>
        /// The application_ start.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            var container = new ContainerBuilder();
            this.InitializeRegistration(container);
            IContainer c = container.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(c));
        }

        /// <summary>
        /// The initialize registration.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        private void InitializeRegistration(ContainerBuilder container)
        {
            container.RegisterType<ETCDataContext>();
            container.RegisterType<UserService>();
            
            container.Register(c => new UserService(c.Resolve<ETCDataContext>())).As<IUserService>();

            container.Register(c => new AuthenticationService(c.Resolve<ETCDataContext>())).As<IAuthenticationService>();
            container.Register(c => new RegistrationService(c.Resolve<ETCDataContext>())).As<IRegistrationService>();
            container.Register(c => new ReportService(c.Resolve<IUserService>())).As<IReportService>();
            container.Register(c => new AccountController(c.Resolve<IAuthenticationService>(), c.Resolve<IRegistrationService>(), c.Resolve<IUserService>()));
            container.Register(c => new ReportController(c.Resolve<IReportService>(), c.Resolve<IUserService>()));
        }

        #endregion
    }
}