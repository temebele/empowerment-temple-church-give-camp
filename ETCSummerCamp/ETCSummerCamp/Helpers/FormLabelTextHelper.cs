﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ETCSummerCamp.Models;

namespace ETCSummerCamp.Helpers
{
    public static class Helpers
    {
        public static MvcHtmlString JsonSerialize(
            this HtmlHelper<RegistrationModel> htmlHelper,
            RegistrationModel model)
        {
            IsoDateTimeConverter conv = new IsoDateTimeConverter();
            conv.DateTimeFormat = "MM/dd/yyyy";
            return MvcHtmlString.Create(JsonConvert.SerializeObject(model, conv));
        }
    }
}