﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;
using ETCSummerCamp.Core.Entities;

namespace ETCSummerCamp.Models
{
    public class ContactsModel
    {
        private const int CONTACT_LIMIT = 10;

        public ContactsModel()
        {
            Contacts = new Collection<ContactModel>();    
            for (int i = 0; i < CONTACT_LIMIT; i++)
            {
                Contacts.Add(new ContactModel());
            }
        }

        public ContactModel EmergencyContact { get; set; }
        public Collection<ContactModel> Contacts { get; set; }

        public void InjectPocoObjects(User user)
        {
            Collection<Contact> results = new Collection<Contact>();
            user.Contacts = new Collection<Contact>();

            if (EmergencyContact != null)
            {
                EmergencyContact.InjectPocoObject(user, ContactModel.ContentTypes.Emergency);             
            }

            foreach (ContactModel cModel in Contacts)
            {
                if (cModel.IsValidContact)
                {
                    cModel.InjectPocoObject(user, ContactModel.ContentTypes.Authorized);
                }                 
            }
        }
    }
}