﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ETCSummerCamp.Core.Entities;

namespace ETCSummerCamp.Models
{
    public class QuestionaireModel
    {
        public int ResponseId { get; set; }

        [Required]
        public int QuestionId { get; set; }

        [Required]
        [Display(Name = "medical question")]
        public bool QuestionResponse { get; set; }

        public string QuestionDescription { get; set; }

        public void InjectPocoObject(User user)
        {
            UserMedicalResponse result = new UserMedicalResponse();
            result.User = user;
            result.Question = new Questionaire{
                QuestionaireId = QuestionId,
            };

            result.UserResponse = QuestionResponse;
            user.UserMedicalResponses.Add(result);
        }
    }
}