﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ETCSummerCamp.Core.Entities;


namespace ETCSummerCamp.Models
{
    public class ContactModel
    {
        public enum ContentTypes
        {
            Emergency,
            Medical,
            Authorized,
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Relationship { get; set; }
        public string Phone { get; set; }
        public string SpecialInstructions { get; set; }

        public bool IsValidContact
        {
            get
            {
                return Name != null;
            }
        }
        
        public void InjectPocoObject(User user, ContentTypes contentType)
        {
            Contact contact = new Contact();
            contact.ContactId = Id;
            contact.User = user;

            switch (contentType)
            {
                case ContentTypes.Emergency:
                    contact.FirstName = Name;
                    contact.Relationship = Relationship;
                    contact.Phone = Phone;
                    contact.ContactTypeId = 1;
                    break;
                case ContentTypes.Medical:
                    //TODO Not supported. This is directly set by medical info model
                    break;
                case ContentTypes.Authorized:
                    contact.FirstName = Name;
                    contact.Relationship = Relationship;
                    contact.SpecialInstruction = SpecialInstructions;
                    contact.ContactTypeId = 3;
                    break;
                default:
                    break;
            }

            user.Contacts.Add(contact);          
        }
    }

    public class SignatureModel
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }
}