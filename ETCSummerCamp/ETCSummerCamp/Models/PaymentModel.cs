﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;
using ETCSummerCamp.Core.Entities;

namespace ETCSummerCamp.Models
{
    public class PaymentModel
    {
        public SignatureModel Camper { get; set; }
        public SignatureModel Parent { get; set; }
        public int PaymentPlan { get; set; }

        public void InjectPocoObject(User user)
        {
            UserPayment newPayment = new UserPayment
            {
                CamperFirstName = this.Camper.FirstName,
                CamperLastName = this.Camper.LastName,
                CamperMiddleName = this.Camper.MiddleName,
                ParentFirstName = this.Parent.FirstName,
                ParentMiddleName = this.Parent.MiddleName,
                ParentLastName = this.Parent.LastName,
                UserPaymentId = this.PaymentPlan
            };

            user.UserPayments = new Collection<UserPayment>();
            user.UserPayments.Add(newPayment);
        }
    }
}