﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ETCSummerCamp.Core.Entities;

namespace ETCSummerCamp.Models
{
    public class StepOneModel
    {
        public StepOneModel()
        {
            BirthDate = DateTime.Now;
        }

        [Required]
        [Display(Name = "user name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "password")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "student name")]
        public string StudentName { get; set; }

        [Required]
        [Display(Name = "email address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "age")]
        public int Age { get; set; }

        [Required]
        [Display(Name = "birth date")]
        public DateTime BirthDate { get; set; }

        [Required]
        [Display(Name = "gender")]
        public string Gender { get; set; }

        [Required]
        [Display(Name = "ethnicity")]
        public string Ethnicity { get; set; }

        [Required]
        [Display(Name = "parent")]
        public string Parent { get; set; }

        [Required]
        [Display(Name = "address")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "city")]
        public string City { get; set; }

        [Required]
        [Display(Name = "state")]
        public string State { get; set; }

        [Required]
        [Display(Name = "zip code")]
        public string ZipCode { get; set; }

        [Required]
        [Display(Name = "home phone")]
        public string HomePhone { get; set; }

        [Required]
        [Display(Name = "alternate phone")]
        public string AlternativePhone { get; set; }

        [Required]
        [Display(Name = "shirt size")]
        public string ShirtSize { get; set; }

        [Required]
        [Display(Name = "grade")]
        public string Grade { get; set; }

        public User GetPocoObject()
        {
            UserDetail details = new UserDetail
            {
                Age = this.Age,
                BirthDate = this.BirthDate,
                City = this.City,
                Email = this.Email,
                Ethnicity = this.Ethnicity,
                Gender = this.Gender,
                Grade = this.Grade,
                Name = this.Grade,
                Parent = this.Parent,
                Phone1 = this.HomePhone,
                Phone2 = this.AlternativePhone,
                State = this.State,
                StreetAddress = this.Address,
                TShirtSize = this.ShirtSize,
                Zip = this.ZipCode
            };

            User cUser = new User
            {
                UserName = this.UserName,
                Password = this.Password,
                UserDetail = details
            };

            return cUser;
        }
    }
}
