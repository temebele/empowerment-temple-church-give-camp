﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Collections.ObjectModel;
using ETCSummerCamp.Core.Entities;

namespace ETCSummerCamp.Models
{
    public class MedicalInfoModel
    {
        public int MedicalContactId { get; set; }

        [Required]
        [Display(Name="doctor")]
        public string Doctor { get; set; }

        [Required]
        [Display(Name="phone")]
        public string Phone { get; set; }

        [Required]
        [Display(Name="insurance")]
        public string Insurance { get; set; }

        [Required]
        [Display(Name="policy number")]
        public string PolicyNumber { get; set; }

        [Required]
        [Display(Name="questionaires")]
        public Collection<QuestionaireModel> Questionaires { get; set; }

        [Display(Name="medical reason")]
        public string MedicalReason { get; set; }

        public void InjectPocoObject(User user)
        {
            user.UserDetail.AdditionalComment = MedicalReason;

            Contact medUser = new Contact
            {
                ContactId = this.MedicalContactId,
                Doctor = this.Doctor,
                Phone = this.Phone,
                InsuranceCarrier = this.Insurance,
                PolicyNumber = this.PolicyNumber,
                ContactTypeId = 2,
                User = user
            };

            user.UserMedicalResponses = new Collection<UserMedicalResponse>();
            foreach (QuestionaireModel qModel in Questionaires)
            {
                qModel.InjectPocoObject(user);                
            }

            user.Contacts.Add(medUser);
        }
    }
}