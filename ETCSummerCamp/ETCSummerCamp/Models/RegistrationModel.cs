﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ETCSummerCamp.Core.Entities;
using System.Collections.ObjectModel;

namespace ETCSummerCamp.Models
{
    public class RegistrationModel
    {
        public RegistrationModel() {
            StepOne = new StepOneModel();
            StepOne.BirthDate = DateTime.Now;
            MedicalInfo = new MedicalInfoModel();
        }

        public StepOneModel StepOne { get; set; }
        public MedicalInfoModel MedicalInfo { get; set; }
        public ContactsModel ContactInfo { get; set; }
        public PaymentModel Payment { get; set; }

        public User GetPocoObject()
        {
            User user = StepOne.GetPocoObject();
            ContactInfo.InjectPocoObjects(user);
            MedicalInfo.InjectPocoObject(user);
            Payment.InjectPocoObject(user);           

            return user;
        }
    }
}